#ifndef THERAPYDIALOG_H
#define THERAPYDIALOG_H
#include <QDateEdit>
#include <QDialog>


class TherapyDialog : public QDialog {
    Q_OBJECT
private:
    QDateEdit* admission;
    QDateEdit*  discharge;

public:
    TherapyDialog(QWidget* pwgt = 0);
    QDate getAdmission() const;
    QDate getDischarge() const;
};

#endif // THERAPYDIALOG_H
