#include "mainwindow.h"
#include "patientDialog.h"
#include "therapyDialog.h"
#include "ui_mainwindow.h"

#include <QApplication>
#include <QBuffer>
#include <QDataWidgetMapper>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QInputDialog>
#include <QMessageBox>
#include <QScreen>
#include <QStandardItem>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    db = new DataBase();
    db->connectToDataBase();
    initUI();
    this->showPatients();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setupModel(const QString &tableName, const QStringList &headers)
{
    model = new QSqlTableModel(this);
    model->setTable(tableName);
    for(int i = 0, j = 0; i < model->columnCount(); i++, j++){
        model->setHeaderData(i,Qt::Horizontal,headers[j]);
    } 
}


void MainWindow::showPatients()
{
    ui->nameEdit->setText("");
    ui->addressEdit->setText("");
    this->setupModel(PATIENTTABLE,
                     QStringList() << "id"
                                   << "Имя"
                                   << "Год рождения"
                                   << "Адрес");
    disableButtons();
    ui->tableView->setModel(model);
    showColumns();
    connect(ui->tableView->selectionModel(), SIGNAL(currentRowChanged(QModelIndex,QModelIndex)),
            this, SLOT(slotCurrentPatient(QModelIndex)));
    model->select();
}

void MainWindow::showTherapys(const int &id)
{
    QString viewName = db->getTherapiesById(id);
    this->setupModel(viewName,
                     QStringList() << "id"
                                   << "id"
                                   << "Дата поступления"
                                   << "Дата выписки" );
    ui->tableView->setModel(model);
    showColumns();
    ui->tableView->setColumnHidden(1, true);    // Hide the column id Records
    connect(ui->tableView->selectionModel(), SIGNAL(currentRowChanged(QModelIndex,QModelIndex)),
            this, SLOT(slotCurrentTherapy(QModelIndex)));
    model->select();
    db->dropView(viewName);
}


void MainWindow::showXRays(const int &id)
{
    QString viewName = db->getPicsById(id);
    this->setupModel( viewName ,
                     QStringList() << "id"
                                   << "id"
                                   << "Дата"
                                   << "Pic"
                                   );
    ui->tableView->setModel(model);
    showColumns();
    ui->tableView->setColumnHidden(1, true);    // Hide the column id Records
    ui->tableView->setColumnHidden(3, true);    // Hide the column id Records
    connect(ui->tableView->selectionModel(), SIGNAL(currentRowChanged(QModelIndex,QModelIndex)),
            this, SLOT(slotCurrentPic(QModelIndex)));
    model->select();
    db->dropView(viewName);
}

void MainWindow::slotCurrentPic(QModelIndex index)
{
    ui->labelID->setNum(model->data(model->index(index.row(), 0)).toInt());
    QPixmap outPixmap = QPixmap();
    outPixmap.loadFromData(model->data(model->index(index.row(), 3)).toByteArray());
    setPic(outPixmap);
}

void MainWindow::slotCurrentTherapy(QModelIndex index)
{
   ui->labelID->setNum(model->data(model->index(index.row(), 0)).toInt());
}

void MainWindow::on_nameEdit_textEdited(const QString &arg1)
{
    int id = ui->labelID->text().toInt();
    db->updateName(id, arg1);
    model->select();
}

void MainWindow::on_yearEdit_valueChanged(int arg1)
{
    int id = ui->labelID->text().toInt();
    db->updateYear(id, arg1);
    model->select();
}

void MainWindow::on_addressEdit_textEdited(const QString &arg1)
{
    int id = ui->labelID->text().toInt();
    db->updateAddress(id, arg1);
    model->select();
}

void MainWindow::on_allPatientsButton_clicked()
{
      ui->labelPatientID->setNum(0);
       ui->labelID->setNum(0);
      this->showPatients();
}

void MainWindow::on_therapysButton_clicked()
{
    int id = ui->labelID->text().toInt();
    this->showTherapys(id);
}

void MainWindow::addXray()
{
    QString filename =  QFileDialog::getOpenFileName(
              this,
              "Open Document",
              QDir::currentPath(),
              " JPEG files (*.jpg);; PNG files (*.png)");
    if(!filename.isEmpty())
        {
            QImage image(filename);

            if(image.isNull())
            {
                QMessageBox::information(this,"Image Viewer","Error Displaying image");
                return;
            }
    QPixmap inPixmap;
    inPixmap.convertFromImage(image, Qt::AutoColor); // Keeping it in the image of the object QPixmap
    QByteArray inByteArray;                             // Create QByteArray object to save the image
    QBuffer inBuffer( &inByteArray );                   // Saving images produced through the buffer
    inBuffer.open( QIODevice::WriteOnly );              // Open buffer
    inPixmap.save( &inBuffer, "PNG" );                  // Write inPixmap in inByteArray
    int id = ui->labelPatientID->text().toInt();
    db->insertIntoPicTable( id, QDateTime::currentDateTime().date(), inByteArray);
    model->select();
    this->showXRays(id);
}
}

void MainWindow::on_xraysButton_clicked()
{
    int id = ui->labelID->text().toInt();
    this->showXRays(id);
}

void MainWindow::initUI(){
    ui->setupUi(this);
    ui->labelID->hide();
    ui->labelPatientID->hide();
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView->horizontalHeader()->setStretchLastSection(true);
}

void MainWindow::disableButtons(){
    ui->therapysButton->setEnabled(false);
    ui->xraysButton->setEnabled(false);
}

void MainWindow::enableButtons(){
    ui->therapysButton->setEnabled(true);
    ui->xraysButton->setEnabled(true);
}

void MainWindow::showColumns(){
    ui->tableView->setColumnHidden( 0, true);
    for(int i = 1; i < model->columnCount(); i++)
        ui->tableView->setColumnHidden( i, false);
    ui->picLabel->clear();
}

void MainWindow::slotCurrentPatient(QModelIndex index)
{
    enableButtons();
    ui->labelID->setNum(model->data(model->index(index.row(), 0)).toInt());
    ui->labelPatientID->setNum(model->data(model->index(index.row(), 0)).toInt());
    ui->nameEdit->setText( model->data(model->index(index.row(), 1)).toString());
    ui->yearEdit->setValue( model->data(model->index(index.row(), 2)).toInt());
    ui->addressEdit->setText( model->data(model->index(index.row(), 3)).toString());
}

void MainWindow::setPic(QPixmap outPixmap){
   int height = ui->centralWidget->height();
   int width = ui->centralWidget->width()/2;
   if(ui->picLabel->height() < height)
        ui->picLabel->setGeometry(ui->picLabel->geometry().x(),ui->picLabel->geometry().y(), ui->picLabel->width() ,height);
   if(ui->picLabel->width() < width)
        ui->picLabel->setGeometry(ui->picLabel->geometry().x(),ui->picLabel->geometry().y(), width ,ui->picLabel->height());
   if(outPixmap.height() > outPixmap.width())
       ui->picLabel->setPixmap(outPixmap.scaledToHeight(ui->picLabel->height()));
   else
       ui->picLabel->setPixmap(outPixmap.scaledToWidth(ui->picLabel->width()));
}

void MainWindow::on_deleteButton_clicked()
{
    int id = ui->labelID->text().toInt();
    int patientID = ui->labelID->text().toInt();
    QString table = model->tableName();
    if(table == PATIENTTABLE){
        db->deletePatient(id);
        model->select();
        showPatients();
    }
    else if(table == THERAPYVIEW){
       db->deleteTherapy(id);
       model->select();
       showTherapys(patientID);
    }
    else if(table == PICVIEW){
       db->deletePic(id);
       model->select();
       showXRays(patientID);
    }
}

void MainWindow::on_addButton_clicked()
{
    int id = ui->labelID->text().toInt();
    int patientID = ui->labelID->text().toInt();
    QString table = model->tableName();
    if(table == PATIENTTABLE){
        PatientDialog* pInputDialog = new PatientDialog;
        if (pInputDialog->exec() == QDialog::Accepted) {
            db->insertIntoPatientTable(pInputDialog->getFio(),pInputDialog->getYear(),pInputDialog->getAddress());

        }
        delete pInputDialog;
       model->select();
       showPatients();
    }
    else if(table == THERAPYVIEW){
        TherapyDialog* pInputDialog = new TherapyDialog;
        if (pInputDialog->exec() == QDialog::Accepted) {
            db->insertIntoTherapyTable(patientID, pInputDialog->getAdmission(),pInputDialog->getDischarge());
        }
       delete pInputDialog;
       model->select();
       showTherapys(patientID);
    }
    else if(table == PICVIEW){
       addXray();
    }
}
