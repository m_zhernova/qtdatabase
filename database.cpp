#include "database.h"

DataBase::DataBase(QObject *parent) : QObject(parent)
{
}

DataBase::~DataBase()
{
}

void DataBase::connectToDataBase()
{
    if(!QFile("" DATABASE_NAME).exists()){
        this->restoreDataBase();
    } else {
        this->openDataBase();
    }
}

bool DataBase::restoreDataBase()
{
    if(this->openDataBase()){
        return (this->createPicTable() && this->createPatientTable() && this->createTherapyTable()) ? true : false;
    } else {
        qDebug() << "Не удалось восстановить базу данных";
        return false;
    }
}


bool DataBase::insertIntoPicTable(const int &idPatient, const QDate &date, const QByteArray &pic)
{
    QVariantList data;
    data.append(idPatient);
    data.append(date);
    data.append(pic);

    if(insertIntoPicTable(data))
        return true;
    else
        return false;
}


bool DataBase::insertIntoPatientTable(const QString &fio, const int &year, const QString &address)
{
    QVariantList data;
    data.append(fio);
    data.append(year);
    data.append(address);
    if(insertIntoPatientTable(data))
        return true;
    else
        return false;
}

bool DataBase::insertIntoTherapyTable(const int &idPatient, const QDate &admission, const QDate &discharge)
{
    QVariantList data;
    data.append(idPatient);
    data.append(admission);
    data.append(discharge);
    if(insertIntoTherapyTable(data))
        return true;
    else
        return false;
}

bool DataBase::openDataBase()
{
    //----------------------data--layer---------------------------
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setHostName(DATABASE_HOSTNAME);
    db.setDatabaseName("" DATABASE_NAME);  
    if(db.open()){
        QSqlQuery query;
        query.exec("PRAGMA foreign_keys = ON;");
        return true;
    } else {
        return false;
    }
    //----------------------data--layer---------------------------
}

void DataBase::closeDataBase()
{
     //----------------------data--layer---------------------------
    db.close();
     //----------------------data--layer---------------------------
}

bool DataBase::createPatientTable()
{
    //----------------------data--layer---------------------------
    QSqlQuery query;
    if(!query.exec( "CREATE TABLE " PATIENTTABLE " ("
                            PATIENTTABLE_ID " INTEGER PRIMARY KEY AUTOINCREMENT, "
                            PATIENTTABLE_FIO     " VARCHAR(255)    NOT NULL,"
                            PATIENTTABLE_YEAR      " INT            NOT NULL,"
                            PATIENTTABLE_ADDRESS      " VARCHAR(255)            NOT NULL"
                    " )"
                    )){
        qDebug() << "DataBase: error of create " << PICTABLE;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
    //----------------------data--layer---------------------------
}

bool DataBase::insertIntoPatientTable(const QVariantList &data)
{
     //----------------------data--layer---------------------------
    QSqlQuery query;
    query.prepare("INSERT INTO " PATIENTTABLE " ( " PATIENTTABLE_FIO ", "
                                             PATIENTTABLE_YEAR ", "
                                             PATIENTTABLE_ADDRESS " ) "
                  "VALUES (:fio, :year, :address)");
    query.bindValue(":fio", data[0].toString());
    query.bindValue(":year",data[1].toInt());
    query.bindValue(":address", data[2].toString());
    if(!query.exec()){
        qDebug() << "error insert into " << PATIENTTABLE;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
     //----------------------data--layer---------------------------
}

bool DataBase::updateName(const int &id, const QString &fio){
     //----------------------data--layer---------------------------
    QSqlQuery query;
    query.prepare("UPDATE " PATIENTTABLE " SET " PATIENTTABLE_FIO " = :fio WHERE " PATIENTTABLE_ID " = :id ");
    query.bindValue(":fio", fio);
    query.bindValue(":id", id);
    if(!query.exec()){
        qDebug() << "error update " << PATIENTTABLE;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
     //----------------------data--layer---------------------------
}

bool DataBase::updateYear(const int &id, const int &year){
     //----------------------data--layer---------------------------
    QSqlQuery query;
    query.prepare("UPDATE " PATIENTTABLE " SET " PATIENTTABLE_YEAR " = :year WHERE " PATIENTTABLE_ID " = :id ");
    query.bindValue(":year", year);
    query.bindValue(":id", id);
    if(!query.exec()){
        qDebug() << "error update " << PATIENTTABLE;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
     //----------------------data--layer---------------------------
}

bool DataBase::updateAddress(const int &id, const QString &address){

    //----------------------data--layer---------------------------

    QSqlQuery query;
    query.prepare("UPDATE " PATIENTTABLE " SET " PATIENTTABLE_ADDRESS " = :address WHERE " PATIENTTABLE_ID " = :id ");
    query.bindValue(":address", address);
    query.bindValue(":id", id);
    if(!query.exec()){
        qDebug() << "error update " << PATIENTTABLE;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }

    //----------------------data--layer---------------------------
}
bool DataBase::deletePatient(const int &id){
     //----------------------data--layer---------------------------
    QSqlQuery query;
    query.prepare(" DELETE FROM " PATIENTTABLE " WHERE " PATIENTTABLE_ID " = :id ");
    query.bindValue(":id", id);
    if(!query.exec()){
        qDebug() << "error DELETE " << PATIENTTABLE;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
     //----------------------data--layer---------------------------
}

bool DataBase::deleteTherapy(const int &id){
     //----------------------data--layer---------------------------
    QSqlQuery query;
    query.prepare(" DELETE FROM " THERAPYTABLE " WHERE " THERAPYTABLE_ID " = :id ");
    query.bindValue(":id", id);
    if(!query.exec()){
        qDebug() << "error update " << PATIENTTABLE;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
     //----------------------data--layer---------------------------
}


bool DataBase::deletePic(const int &id){
    QSqlQuery query;
    query.prepare(" DELETE FROM " PICTABLE " WHERE " PICTABLE_ID " = :id ");
    query.bindValue(":id", id);
    if(!query.exec()){
        qDebug() << "error update " << PATIENTTABLE;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
}


bool DataBase::createTherapyTable()
{
    QSqlQuery query;
    if(!query.exec( "CREATE TABLE " THERAPYTABLE " ("
                            THERAPYTABLE_ID " INTEGER PRIMARY KEY AUTOINCREMENT, "
                            PATIENTTABLE_ID " INTEGER, "
                            THERAPYTABLE_DATE_ADMISSION     " DATE    NOT NULL,"
                            THERAPYTABLE_DATE_DISCHARGE     " DATE ,"
                            " FOREIGN KEY ( " PATIENTTABLE_ID " ) REFERENCES " PATIENTTABLE " ( " PATIENTTABLE_ID " ) ON DELETE CASCADE"
                    " )"
                    )){
        qDebug() << "DataBase: error of create " << PICTABLE;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
}

bool DataBase::insertIntoTherapyTable(const QVariantList &data)
{
    QSqlQuery query;
    query.prepare("INSERT INTO " THERAPYTABLE " ( " PATIENTTABLE_ID " , "
                                             THERAPYTABLE_DATE_ADMISSION  " , "
                                             THERAPYTABLE_DATE_DISCHARGE  " ) "
                  "VALUES (:patient , :adm , :dis )");

    query.bindValue(":patient" ,        data[0].toInt());
    query.bindValue(":adm" ,  data[1].toDate());
    query.bindValue(":dis"  , data[2].toDate());
    if(!query.exec()){
        qDebug() << "error insert into " << PATIENTTABLE;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
}

bool DataBase::createPicTable()
{
    QSqlQuery query;
    if(!query.exec( "CREATE TABLE " PICTABLE " ("
                            PICTABLE_ID " INTEGER PRIMARY KEY AUTOINCREMENT, "
                            PATIENTTABLE_ID " INTEGER, "
                            PICTABLE_DATE     " VARCHAR(255)    NOT NULL,"
                            PICTABLE_PIC      " BLOB            NOT NULL,"
                    " FOREIGN KEY ( " PATIENTTABLE_ID " ) REFERENCES " PATIENTTABLE " ( " PATIENTTABLE_ID " ) ON DELETE CASCADE"
                        " )"
                    )){
        qDebug() << "DataBase: error of create " << PICTABLE;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
}

bool DataBase::insertIntoPicTable(const QVariantList &data)
{
    QSqlQuery query;
    query.prepare("INSERT INTO " PICTABLE " ( " PATIENTTABLE_ID ", "
                                            PICTABLE_DATE ", "
                                             PICTABLE_PIC " ) "
                  "VALUES (:patient, :date, :pic)");
    query.bindValue(":patient"  ,         data[0].toInt());
    query.bindValue(":date"  ,         data[1].toDate());
    query.bindValue(":pic" ,         data[2].toByteArray());
    if(!query.exec()){
        qDebug() << "error insert into " << PICTABLE;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
}

QString DataBase::getTherapiesById(const int& id)
{
    QSqlQuery query;
    QString q;
    q =  "CREATE VIEW " THERAPYVIEW " AS SELECT * FROM " THERAPYTABLE " WHERE " PATIENTTABLE_ID " = ";
    q.append( QString::number( id ) );
    query.prepare(q);
    qDebug() << query.lastError().text();
    if(!query.exec()){
    qDebug() << "error CREATE VIEW " << THERAPYTABLE;
    qDebug() << query.lastError().text();
    return "";
}
    else {
        return THERAPYVIEW;
    }
}

QString DataBase::getPicsById(const int& id)
{
QSqlQuery query;
QString q;
q =  "CREATE VIEW " PICVIEW " AS SELECT * FROM " PICTABLE " WHERE " PATIENTTABLE_ID " = ";
q.append( QString::number( id ) );
query.prepare(q);
qDebug() << query.lastError().text();
if(!query.exec()){
    qDebug() << "error CREATE VIEW " << THERAPYTABLE;
    qDebug() << query.lastError().text();
    return "";
}
    else {
        return PICVIEW;
    }
}

bool DataBase::dropView(const QString &view)
{
    QSqlQuery query;
    QString q;
    q =  "DROP VIEW ";
    q.append(view);
    query.prepare(q);
    if(!query.exec()){
        qDebug() << "error DROP VIEW " << THERAPYTABLE;
        qDebug() << query.lastError().text();
        return false;
    }
    return true;
}
