#ifndef DIALOG_H
#define DIALOG_H
#include <QDialog>
#include <qspinbox.h>

class PatientDialog : public QDialog {
    Q_OBJECT
private:
    QLineEdit* fio;
    QSpinBox*  year;
    QLineEdit* address;

public:
    PatientDialog(QWidget* pwgt = 0);
    QString getFio() const;
    int getYear() const;
    QString getAddress () const;
};

#endif // DIALOG_H
