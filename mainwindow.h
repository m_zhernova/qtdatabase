#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlTableModel>
#include <QModelIndex>

#include "database.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private slots:
    // The slot for the getting of image from the database
    void slotCurrentPic(QModelIndex index);
    void slotCurrentPatient(QModelIndex index);
    void on_nameEdit_textEdited(const QString &arg1);
    void on_yearEdit_valueChanged(int arg1);
    void on_addressEdit_textEdited(const QString &arg1);
    void on_allPatientsButton_clicked();
    void on_therapysButton_clicked();
    void on_xraysButton_clicked();
    void on_deleteButton_clicked();
    void slotCurrentTherapy(QModelIndex index);
    void on_addButton_clicked();

private:
    Ui::MainWindow *ui;
    DataBase        *db;
    QSqlTableModel  *model;

private:
    void setupModel(const QString &tableName, const QStringList &headers);
    void showPatients();
    void showTherapys(const int &id);
    void showXRays(const int &id);
    void disableButtons();
    void enableButtons();
    void showColumns();
    void setPic(QPixmap outPixmap);
    void initUI();
    void addXray();
};

#endif // MAINWINDOW_H
