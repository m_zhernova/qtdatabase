#include <QtGui>
#include <qgridlayout.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include "patientDialog.h"

PatientDialog::PatientDialog(QWidget* pwgt/*= 0*/)
     : QDialog(pwgt, Qt::WindowTitleHint | Qt::WindowSystemMenuHint)
{
    fio = new QLineEdit;
    year = new QSpinBox;
    address  = new QLineEdit;

    year->setRange(1920,2019);
    year->setValue(2000);

    QLabel* fioLabel    = new QLabel("&ФИО");
    QLabel* yearLabel    = new QLabel("&Год рождения");
    QLabel* addressLabel     = new QLabel("&Адрес");

    fioLabel->setBuddy(fio);
    yearLabel ->setBuddy(year);
    addressLabel->setBuddy(address);

    QPushButton* pcmdOk     = new QPushButton("&Ok");
    QPushButton* pcmdCancel = new QPushButton("&Cancel");

    connect(pcmdOk, SIGNAL(clicked()), SLOT(accept()));
    connect(pcmdCancel, SIGNAL(clicked()), SLOT(reject()));

    //Layout setup
    QGridLayout* ptopLayout = new QGridLayout;
    ptopLayout->addWidget(fioLabel, 0, 0);
    ptopLayout->addWidget(yearLabel, 1, 0);
    ptopLayout->addWidget(addressLabel, 2, 0);
    ptopLayout->addWidget(fio, 0, 1);
    ptopLayout->addWidget(year, 1, 1);
    ptopLayout->addWidget(address, 2, 1);
    ptopLayout->addWidget(pcmdOk, 3,0);
    ptopLayout->addWidget(pcmdCancel, 3, 1);
    setLayout(ptopLayout);
}

QString PatientDialog::getFio() const
{
    return fio->text();
}

int PatientDialog::getYear() const
{
    return year->value();
}

QString PatientDialog::getAddress() const
{
    return address->text ();
}
