#include "therapyDialog.h"

#include <qgridlayout.h>
#include <qlabel.h>
#include <qpushbutton.h>

TherapyDialog::TherapyDialog(QWidget* pwgt/*= 0*/)
     : QDialog(pwgt, Qt::WindowTitleHint | Qt::WindowSystemMenuHint)
{
   admission = new   QDateEdit;
   discharge = new   QDateEdit;

   admission->setDate(QDateTime::currentDateTime().date());
   admission->setEnabled(false);
   discharge->setDateRange(QDateTime::currentDateTime().date(),QDateTime::currentDateTime().date().addDays(14));

    QLabel* admissionLabel    = new QLabel("&Дата поступления");
    QLabel* dischargeLabel    = new QLabel("&Дата выписки");

    admissionLabel->setBuddy( admission);
    dischargeLabel ->setBuddy(discharge);

    QPushButton* pcmdOk     = new QPushButton("&Ok");
    QPushButton* pcmdCancel = new QPushButton("&Cancel");

    connect(pcmdOk, SIGNAL(clicked()), SLOT(accept()));
    connect(pcmdCancel, SIGNAL(clicked()), SLOT(reject()));

    //Layout setup
    QGridLayout* ptopLayout = new QGridLayout;
    ptopLayout->addWidget(admissionLabel, 0, 0);
    ptopLayout->addWidget(dischargeLabel, 1, 0);
    ptopLayout->addWidget(admission, 0, 1);
    ptopLayout->addWidget(discharge, 1, 1);
    ptopLayout->addWidget(pcmdOk, 2,0);
    ptopLayout->addWidget(pcmdCancel, 2, 1);
    setLayout(ptopLayout);
}

QDate TherapyDialog::getAdmission() const
{
    return admission->date();
}

QDate TherapyDialog::getDischarge() const
{
       return discharge->date();
}
