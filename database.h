#ifndef DATABASE_H
#define DATABASE_H
#include <QObject>
#include <QSql>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlDatabase>
#include <QFile>
#include <QDate>
#include <QDebug>

#define DATABASE_HOSTNAME   "ScreenDataBase"
#define DATABASE_NAME       "Screen.db"

#define PICTABLE                   "PicTable"
#define PICTABLE_ID                "idPic"
#define PICTABLE_DATE              "Date"
#define PICTABLE_PIC               "Pic"

#define PATIENTTABLE               "PatientTable"
#define PATIENTTABLE_ID            "idPatient"
#define PATIENTTABLE_FIO           "FIO"
#define PATIENTTABLE_YEAR          "Year"
#define PATIENTTABLE_ADDRESS       "Address"

#define THERAPYTABLE                "TherapyTable"
#define THERAPYTABLE_ID             "idTherapy"
#define THERAPYTABLE_DATE_ADMISSION "AdmissionDate"
#define THERAPYTABLE_DATE_DISCHARGE "DischargeDate"

#define PICVIEW                   "PicView"
#define THERAPYVIEW               "TherapyView"

class DataBase : public QObject
{
    Q_OBJECT
public:
    explicit DataBase(QObject *parent = nullptr);
    ~DataBase();
    void connectToDataBase();



private:
    QSqlDatabase    db;

private:
    bool openDataBase();
    bool restoreDataBase();
    void closeDataBase();
    bool createPicTable();
    bool createPatientTable();
    bool createTherapyTable();
public slots:
    bool insertIntoPicTable(const QVariantList &data);
    bool insertIntoPatientTable(const QVariantList &data);

    bool insertIntoPatientTable(const QString &fio, const int &year, const QString &address);
    bool insertIntoTherapyTable(const int &idPatient, const QDate &admission, const QDate &discharge);
    bool insertIntoPicTable(const int &idPatient, const QDate &name, const QByteArray &pic);

    bool insertIntoTherapyTable(const QVariantList &data);
    bool updateName(const int &id, const QString &fio);
    bool updateYear(const int &id, const int &year);
    bool updateAddress(const int &id, const QString &address);
    QString getTherapiesById(const int &id);
    bool dropView(const QString &view);
    QString getPicsById(const int &id);
    bool deletePatient(const int &id);
    bool deleteTherapy(const int &id);
    bool deletePic(const int &id);
};
#endif // DATABASE_H
